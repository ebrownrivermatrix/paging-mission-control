package pmc;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

public class SatelliteDataTest {
    @Test
    public void testOneRow() {
        MonitorData md = new MonitorData("src/test/resources/one-row.csv");

        String json = md.alertFromData();

        Assert.assertEquals("Should be empty json value", "[]", json);
    }

    @Test
    public void testBeanParsing() {
        MonitorData md = new MonitorData("src/test/resources/three-rows.csv");

        String json = md.alertFromData();

        System.out.println(json);

        ArrayList<AlertMessage>list = new ArrayList<>();
        AlertMessage am = new AlertMessage();
        am.setSatelliteId(1000);
        am.setSeverity("RED HIGH");
        am.setComponent("TSTAT");
        am.setTimestamp("20180101 23:01:38.001Z");

        list.add(am);

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String testJson = gson.toJson(list);

        Assert.assertEquals("Should be the same", testJson, json);
    }

    @Test
    public void testAllData() {
        MonitorData md = new MonitorData("src/test/resources/test-sat-data.csv");

        String json = md.alertFromData();

        ArrayList<AlertMessage>list = new ArrayList<>();

        AlertMessage m = new AlertMessage();
        m.setSatelliteId(1000);
        m.setSeverity("RED HIGH");
        m.setComponent("TSTAT");
        m.setTimestamp("20180101 23:01:38.001Z");
        list.add(m);

        AlertMessage am = new AlertMessage();
        am.setSatelliteId(1000);
        am.setSeverity("RED LOW");
        am.setComponent("BATT");
        am.setTimestamp("20180101 23:01:09.521Z");
        list.add(am);

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String testJson = gson.toJson(list);

        Assert.assertEquals("Should be the same", testJson, json);
    }

}