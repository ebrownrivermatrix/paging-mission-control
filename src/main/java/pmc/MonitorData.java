package pmc;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.univocity.parsers.common.processor.BeanListProcessor;
import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * Class used to read the CSV file and create the alerts
 */
public class MonitorData {
    /** The list of satellite data read in */
    private List<SatelliteData> satelliteData;

    /**
     * Constructor. Reads in the satellite data
     * @param filePath The file path to the satellite data
     */
    public MonitorData(String filePath) {
        try {
            BeanListProcessor<SatelliteData> rowProcessor = new BeanListProcessor<SatelliteData>(SatelliteData.class);
            CsvParserSettings parserSettings = new CsvParserSettings();
            parserSettings.getFormat().setLineSeparator("\n");
            parserSettings.setProcessor(rowProcessor);
            parserSettings.getFormat().setDelimiter('|');

            FileInputStream fis = new FileInputStream(filePath);

            CsvParser parser = new CsvParser(parserSettings);

            parser.parse(fis);

            satelliteData = rowProcessor.getBeans();

        } catch (FileNotFoundException e) {
            // Instructions say to assume data is properly formatted
        }
    }

    /**
     * Returns alerts the satellites data
     * @return Returns the alert data in JSON form
     */
    public String alertFromData() {
        ArrayList<SatelliteData> sortedTstat = new ArrayList<>();
        ArrayList<SatelliteData> sortedBatt = new ArrayList<>();
        List<AlertMessage>alertList = new ArrayList<AlertMessage>();

        satelliteData.forEach(data -> {
            if (data.getComponent().equals("TSTAT")) {
                sortedTstat.add(data);
            } else if (data.getComponent().equals("BATT")) {
                sortedBatt.add(data);
            }
        });

        sortedTstat.sort((SatelliteData l, SatelliteData r) -> {
            if (l.getTime() < r.getTime()) {
                return -1;
            } else if (l.getTime() == r.getTime()) {
                return 0;
            } else return 1;
        });

        List<AlertMessage>alerts = highHeatLimit(sortedTstat, 5L * 1000 * 60);

        if (alerts.size() > 0) {
            alertList.addAll(alerts);
        }

        sortedBatt.sort((SatelliteData l, SatelliteData r) -> {
            if (l.getTime() < r.getTime()) {
                return -1;
            } else if (l.getTime() == r.getTime()) {
                return 0;
            } else return 1;
        });

        alerts = battLimit(sortedBatt, 5L*1000*60);

        if (alerts.size() > 0) {
            alertList.addAll(alerts);
        }

        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        return gson.toJson(alertList);
    }


    /**
     * Create a map of sorted lists of satellites
     * @param list The list of satellites
     * @return A map of a list of satellites, with the satellite id as the key
     */
    private HashMap<Integer, List<SatelliteData>> sortSatellites(List<SatelliteData>list) {
        HashMap<Integer, List<SatelliteData>> map = new HashMap<>();

        // put in a map based on satellite id
        list.forEach(satellite -> {
            List<SatelliteData> sublist;

            if (map.containsKey(satellite.getSatelliteId())) {
                sublist = map.get(satellite.getSatelliteId());
            } else {
                sublist = new ArrayList<>();
                map.put(satellite.getSatelliteId(), sublist);
            }

            sublist.add(satellite);

        });

        return map;
    }

    /**
     * Calculate the alerts for the satellites with a threshold greater than the high heat limit
     * @param list The list of satellites
     * @param threshold The threshold
     * @return Returns the list of alerts
     */
    private List<AlertMessage> highHeatLimit(List<SatelliteData> list, long threshold) {
        List<AlertMessage> alertList = new ArrayList<AlertMessage>();

        // no need to check if null list or the list isn't big enough
        if (list != null && list.size() >= 3) {
            HashMap<Integer, List<SatelliteData>> map = sortSatellites(list);

            // for each satellite id, compare for the time frame
            Iterator<Integer> satIds = map.keySet().iterator();

            while (satIds.hasNext()) {
                int satId = satIds.next();

                List<SatelliteData> sublist = map.get(satId);

                int size = sublist.size();

                // go thru the satellites
                for (int i = 0; i < sublist.size(); i++) {

                    SatelliteData satellite = sublist.get(i);

                    // start at the first high value and only if 3 or most in the list exist at this point
                    if (satellite.getRawValue() > satellite.getRedHighLimit() && i+2 < sublist.size()) {
                        int alertCounter = 1;

                        for (int j = i+1; j < size; j++) {
                            SatelliteData compareSat = sublist.get(j);

                            if (compareSat.getRawValue() > compareSat.getRedHighLimit() &&
                                    compareSat.getTime() <= (satellite.getTime() + threshold)) {

                                ++alertCounter;
                            } // if (compareSat.getRawValue() > compareSat.getRedHighLimit() &&

                            if (alertCounter >= 3) {
                                AlertMessage alert = new AlertMessage();
                                alert.setComponent(satellite.getComponent());
                                alert.setSatelliteId(satellite.getSatelliteId());
                                alert.setSeverity("RED HIGH");
                                alert.setTimestamp(satellite.getTimestamp() + 'Z');

                                alertList.add(alert);

                                break;
                            }
                        } // for (int j = i+1; j < size; j++) {

                    }

                } // for (int i = 0; i < sublist.size(); i++) {

            } // while (satIds.hasNext()) {
        }

        return alertList;
    }

    /**
     * Calculate the alerts for the satellites with a threshold less than the red battery limit
     * @param list The list of satellites
     * @param threshold The threshold
     * @return Return the list of alerts
     */
    private List<AlertMessage> battLimit(List<SatelliteData> list, long threshold) {
        ArrayList<AlertMessage>alertList = new ArrayList<>();

        // no need to check if null list or the list isn't big enough
        if (list != null && list.size() >= 3) {

            HashMap<Integer, List<SatelliteData>> map = sortSatellites(list);

            // for each satellite id, compare for the time frame
            Iterator<Integer> satIds = map.keySet().iterator();

            // loop thru the map of satellites
            while (satIds.hasNext()) {
                int satId = satIds.next();

                List<SatelliteData> sublist = map.get(satId);

                int size = sublist.size();

                // go thru the satellites in the list of the satellite
                for (int i = 0; i < sublist.size(); i++) {

                    SatelliteData satellite = sublist.get(i);

                    // start at the first low value and only if 3 or most in the list exist at this point
                    if (satellite.getRawValue() < satellite.getRedLowLimit() && i+2 < sublist.size()) {
                        int alertCounter = 1;

                        for (int j = i+1; j < size; j++) {
                            SatelliteData compareSat = sublist.get(j);

                            if (compareSat.getRawValue() < compareSat.getRedLowLimit() &&
                                    compareSat.getTime() <= (satellite.getTime() + threshold)) {

                                ++alertCounter;
                            } // if (compareSat.getRawValue() > compareSat.getRedHighLimit() &&

                            if (alertCounter >= 3) {
                                AlertMessage alert = new AlertMessage();
                                alert.setComponent(satellite.getComponent());
                                alert.setSatelliteId(satellite.getSatelliteId());
                                alert.setSeverity("RED LOW");
                                alert.setTimestamp(satellite.getTimestamp() + 'Z');

                                alertList.add(alert);

                                break;
                            }
                        } // for (int j = i+1; j < size; j++) {
                    }
                } // for (int i = 0; i < sublist.size(); i++) {
            } // while (satIds.hasNext()) {
        }

        return alertList;
    }

}
