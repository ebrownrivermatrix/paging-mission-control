package pmc;


import com.univocity.parsers.annotations.Parsed;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Objects;

/**
 * The class that defines the bean used to read the CSV data
 */
public class SatelliteData implements Comparable<Long> {
    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");

    // <timestamp>|<satellite-id>|<red-high-limit>|<yellow-high-limit>|<yellow-low-limit>|<red-low-limit>|<raw-value>|<component>
    // 20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT

    @Parsed(field="timestamp", index=0)
    private String timestamp;
    @Parsed(field = "satellite-id", index=1)
    private int satelliteId;
    @Parsed(field = "red-high-limit", index = 2)
    private int redHighLimit;
    @Parsed(field = "yellow-high-limit", index=3)
    private int yellowHighLimit;
    @Parsed(field = "yellow-low-limit", index=4)
    private int yellowLowLimit;
    @Parsed(field = "red-low-limit", index=5)
    private int redLowLimit;
    @Parsed(field = "raw-value", index=6)
    private double rawValue;
    @Parsed(field = "component", index=7)
    private String component;

    /** */
    private long time;

    public SatelliteData() {

    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;

        try {
            time = sdf.parse(timestamp).getTime();
        } catch (ParseException e) {
            time = 0;
        }
    }

    public int getSatelliteId() {
        return satelliteId;
    }

    public void setSatelliteId(int satelliteId) {
        this.satelliteId = satelliteId;
    }

    public int getRedHighLimit() {
        return redHighLimit;
    }

    public void setRedHighLimit(int redHighLimit) {
        this.redHighLimit = redHighLimit;
    }

    public int getYellowHighLimit() {
        return yellowHighLimit;
    }

    public void setYellowHighLimit(int yellowHighLimit) {
        this.yellowHighLimit = yellowHighLimit;
    }

    public int getYellowLowLimit() {
        return yellowLowLimit;
    }

    public void setYellowLowLimit(int yellowLowLimit) {
        this.yellowLowLimit = yellowLowLimit;
    }

    public int getRedLowLimit() {
        return redLowLimit;
    }

    public void setRedLowLimit(int redLowLimit) {
        this.redLowLimit = redLowLimit;
    }

    public double getRawValue() {
        return rawValue;
    }

    public void setRawValue(double rawValue) {
        this.rawValue = rawValue;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public long getTime() {
        return time;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("SatelliteData{");
        sb.append("timestamp='").append(timestamp).append('\'');
        sb.append(", satelliteId=").append(satelliteId);
        sb.append(", redHighLimit=").append(redHighLimit);
        sb.append(", yellowHighLimit=").append(yellowHighLimit);
        sb.append(", yellowLowLimit=").append(yellowLowLimit);
        sb.append(", redLowLimit=").append(redLowLimit);
        sb.append(", rawValue=").append(rawValue);
        sb.append(", component='").append(component).append('\'');
        sb.append(", time=").append(time);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SatelliteData that = (SatelliteData) o;
        return satelliteId == that.satelliteId &&
                redHighLimit == that.redHighLimit &&
                yellowHighLimit == that.yellowHighLimit &&
                yellowLowLimit == that.yellowLowLimit &&
                redLowLimit == that.redLowLimit &&
                Double.compare(that.rawValue, rawValue) == 0 &&
                timestamp.equals(that.timestamp) &&
                component.equals(that.component);
    }

    @Override
    public int hashCode() {
        return Objects.hash(timestamp, satelliteId, redHighLimit, yellowHighLimit, yellowLowLimit, redLowLimit, rawValue, component);
    }

    /**
     * Compares this object with the specified object for order.  Returns a
     * negative integer, zero, or a positive integer as this object is less
     * than, equal to, or greater than the specified object.
     *
     * <p>The implementor must ensure <tt>sgn(x.compareTo(y)) ==
     * -sgn(y.compareTo(x))</tt> for all <tt>x</tt> and <tt>y</tt>.  (This
     * implies that <tt>x.compareTo(y)</tt> must throw an exception iff
     * <tt>y.compareTo(x)</tt> throws an exception.)
     *
     * <p>The implementor must also ensure that the relation is transitive:
     * <tt>(x.compareTo(y)&gt;0 &amp;&amp; y.compareTo(z)&gt;0)</tt> implies
     * <tt>x.compareTo(z)&gt;0</tt>.
     *
     * <p>Finally, the implementor must ensure that <tt>x.compareTo(y)==0</tt>
     * implies that <tt>sgn(x.compareTo(z)) == sgn(y.compareTo(z))</tt>, for
     * all <tt>z</tt>.
     *
     * <p>It is strongly recommended, but <i>not</i> strictly required that
     * <tt>(x.compareTo(y)==0) == (x.equals(y))</tt>.  Generally speaking, any
     * class that implements the <tt>Comparable</tt> interface and violates
     * this condition should clearly indicate this fact.  The recommended
     * language is "Note: this class has a natural ordering that is
     * inconsistent with equals."
     *
     * <p>In the foregoing description, the notation
     * <tt>sgn(</tt><i>expression</i><tt>)</tt> designates the mathematical
     * <i>signum</i> function, which is defined to return one of <tt>-1</tt>,
     * <tt>0</tt>, or <tt>1</tt> according to whether the value of
     * <i>expression</i> is negative, zero or positive.
     *
     * @param compTime the object to be compared.
     * @return a negative integer, zero, or a positive integer as this object
     * is less than, equal to, or greater than the specified object.
     * @throws NullPointerException if the specified object is null
     * @throws ClassCastException   if the specified object's type prevents it
     *                              from being compared to this object.
     */
    @Override
    public int compareTo(Long compTime) {
        if (time < compTime ) {
            return -1;
        } else if (time == compTime) {
            return 0;
        }
        else {
            return 1;
        }
    }
}
