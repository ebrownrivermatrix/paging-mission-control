package pmc;

import java.util.Objects;

/**
 * Bean used for the alerting message output as JSON
 */
public class AlertMessage {
    /** satellite id */
    private int satelliteId;
    /** severity */
    private String severity;
    /** component */
    private String component;
    /** time stamp */
    private String timestamp;

    public AlertMessage() {

    }

    public int getSatelliteId() {
        return satelliteId;
    }

    public void setSatelliteId(int satelliteId) {
        this.satelliteId = satelliteId;
    }

    public String getSeverity() {
        return severity;
    }

    public void setSeverity(String severity) {
        this.severity = severity;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AlertMessage that = (AlertMessage) o;
        return satelliteId == that.satelliteId &&
                severity.equals(that.severity) &&
                component.equals(that.component) &&
                timestamp.equals(that.timestamp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(satelliteId, severity, component, timestamp);
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("AlertMessage{");
        sb.append("satelliteId=").append(satelliteId);
        sb.append(", severity='").append(severity).append('\'');
        sb.append(", component='").append(component).append('\'');
        sb.append(", timestamp='").append(timestamp).append('\'');
        sb.append('}');
        return sb.toString();
    }


}
