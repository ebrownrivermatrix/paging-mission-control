package pmc;

import org.apache.commons.cli.*;

/**
 * The main class
 */
public class Main {
    public static void main(String... args) {
        Options options = new Options();

        Option option = new Option("f", "file", true, "The file path");
        option.setRequired(true);

        options.addOption(option);

        try {

            CommandLineParser clp = new DefaultParser();

            CommandLine cl = clp.parse(options, args);

            String path = cl.getOptionValue("file");

            MonitorData md = new MonitorData(path);

            String json = md.alertFromData();

            System.out.println(json);

        } catch (ParseException e) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("MonitorData", options);
            System.exit(1);
        }


    }
}
